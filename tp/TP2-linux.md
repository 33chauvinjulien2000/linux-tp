# Installation mariadb-server

````
[admin@db ~]$ sudo dnf install mariadb
[sudo] password for admin:
Last metadata expiration check: 4:08:20 ago on Tue 14 Dec 2021 10:51:10 AM CET.
Dependencies resolved.
[...]
Complete!
````

# Lancement du service de mariadb

````
[admin@db ~]$ systemctl start mariadb.service
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'mariadb.service'.
Authenticating as: admin
Password:
==== AUTHENTICATION COMPLETE ====
````

# Status mariadb :

````
[admin@db ~]$ systemctl status mariadb.service
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disable>
   Active: active (running) since Tue 2021-12-14 15:11:01 CET; 19min ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 27278 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCE>
  Process: 27143 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited>
  Process: 27119 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 27246 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 4943)
   Memory: 85.9M
   CGroup: /system.slice/mariadb.service
           └─27246 /usr/libexec/mysqld --basedir=/usr

Dec 14 15:11:01 db.tp2.cesi mysql-prepare-db-dir[27143]: See the MariaDB Knowledgebase at ht>
Dec 14 15:11:01 db.tp2.cesi mysql-prepare-db-dir[27143]: MySQL manual for more instructions.
Dec 14 15:11:01 db.tp2.cesi mysql-prepare-db-dir[27143]: Please report any problems at http:>
Dec 14 15:11:01 db.tp2.cesi mysql-prepare-db-dir[27143]: The latest information about MariaD>
Dec 14 15:11:01 db.tp2.cesi mysql-prepare-db-dir[27143]: You can find additional information>
Dec 14 15:11:01 db.tp2.cesi mysql-prepare-db-dir[27143]: http://dev.mysql.com
Dec 14 15:11:01 db.tp2.cesi mysql-prepare-db-dir[27143]: Consider joining MariaDB's strong a>
Dec 14 15:11:01 db.tp2.cesi mysql-prepare-db-dir[27143]: https://mariadb.org/get-involved/
Dec 14 15:11:01 db.tp2.cesi mysqld[27246]: 2021-12-14 15:11:01 0 [Note] /usr/libexec/mysqld >
Dec 14 15:11:01 db.tp2.cesi systemd[1]: Started MariaDB 10.3 database server.
````


# Recherche du port d'écoute : 

````
[admin@db ~]$ sudo ss -lutpn
[sudo] password for admin:
[...]
 users:(("sshd",pid=847,fd=5))
tcp      LISTEN    0          80                         *:3306                    *:*   

````

# détermination des processus lancés par mariadb

````

[admin@db ~]$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
[...]
mysql      27246       1  0 15:11 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr

````

# ouverture du port 3306 via le firewall

````
[admin@db ~]$ sudo firewall-cmd --permanent --add-port=3306/tcp
success
[admin@web ~]$ sudo firewall-cmd --reload
success
````

# my SQL secure installation 

````
[admin@db ~]$ mysql_secure_installation
[...]

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!
[...]
Remove anonymous users? [Y/n] n   (j'aurais du mettre y)
 ... skipping.
[...]
Disallow root login remotely? [Y/n] y
 ... Success!

````



#  Installez sur la machine web.tp2.cesi la commande mysql

````
[admin@web ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                                    1.1 MB/s | 8.4 MB     00:07
Rocky Linux 8 - BaseOS                                       2.9 MB/s | 3.6 MB     00:01
Rocky Linux 8 - Extras                                        14 kB/s |  10 kB     00:00
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared
                                                  : libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

````

# Installer Apache sur la machine web.tp2.cesi :

````

[admin@web ~]$ sudo dnf install httpd

````

# Analyse du service Apache

````

[admin@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 16:18:54 CET; 6min ago
     Docs: man:httpd.service(8)
 Main PID: 24420 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4943)
   Memory: 25.8M
   CGroup: /system.slice/httpd.service
           ├─24420 /usr/sbin/httpd -DFOREGROUND
           ├─24421 /usr/sbin/httpd -DFOREGROUND
           ├─24422 /usr/sbin/httpd -DFOREGROUND
           ├─24423 /usr/sbin/httpd -DFOREGROUND
           └─24424 /usr/sbin/httpd -DFOREGROUND

Dec 14 16:18:53 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 14 16:18:54 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 14 16:18:54 web.tp2.cesi httpd[24420]: Server configured, listening on: port 80


````


On note que son PID est le 24420 : 


````

[admin@web ~]$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD

root       24420       1  0 16:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24421   24420  0 16:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24422   24420  0 16:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24423   24420  0 16:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     24424   24420  0 16:18 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND

````


On cherche le port d'écoute de httpd : 

````

[admin@web ~]$ sudo ss -lutpn
[sudo] password for admin:
Netid   State    Recv-Q   Send-Q     Local Address:Port     Peer Address:Port  Process
tcp     LISTEN   0        128                    *:80                  *:*      users:(("httpd",pid=24424,fd=4),("httpd",pid=24423,fd=4),("httpd",pid=24422,fd=4),("httpd",pid=24420,fd=4))

````

Sous quel utilisateur sont lancés les processus apache ? 

````
[admin@web ~]$ ps -aux | grep httpd
root       24420  0.0  1.4 282936 11644 ?        Ss   16:18   0:00 /usr/sbin/httpd -DFOREGROUND
apache     24421  0.0  1.0 296820  8456 ?        S    16:18   0:00 /usr/sbin/httpd -DFOREGROUND
apache     24422  0.0  1.4 1354604 12240 ?       Sl   16:18   0:00 /usr/sbin/httpd -DFOREGROUND
apache     24423  0.0  1.4 1354604 12240 ?       Sl   16:18   0:00 /usr/sbin/httpd -DFOREGROUND
apache     24424  0.0  1.7 1485732 14288 ?       Sl   16:18   0:00 /usr/sbin/httpd -DFOREGROUND
admin      24686  0.0  0.1 221928  1172 pts/1    R+   16:32   0:00 grep --color=auto httpd

````

Ouverture du port 80 sur le firewall : 

````
[admin@web ~]$ sudo firewall-cmd --permanent --add-port=80/tcp
[sudo] password for admin:
success
[admin@web ~]$ sudo firewall-cmd --reload
success

````

Test d'accés à la page web : 

- accés en page web OK

````
[admin@web ~]$ curl 10.2.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
[...]

````



# Installer PHP 

````
[admin@web ~]$ sudo dnf install epel-release
Complete!
[admin@web ~]$ sudo dnf update
Complete!
[admin@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Complete!
[admin@web ~]$ dnf module enable php:remi-7.4
Complete!
[admin@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Complete!

````


# Analyser la conf apache : 

````
[admin@web conf]$ sudo nano httpd.conf
[...]
IncludeOptional conf.d/*.conf

````

# aprés écriture d'un fichier.conf dans le drop-in

````
[admin@web conf.d]$ systemctl restart httpd

==== AUTHENTICATION COMPLETE ====
Warning: The unit file, source configuration file or drop-ins of httpd.service changed on disk. Run 'systemctl daemon-reload' to reload units.
[admin@web conf.d]$

````

# Configurer la racine web :

````
[admin@web www]$ mkdir nextcloud
mkdir: cannot create directory ‘nextcloud’: Permission denied
[admin@web www]$ sudo mkdir nextcloud
[admin@web www]$ cd nextcloud/
[admin@web nextcloud]$ sudo mkdir html
[admin@web html]$ sudo chown -R apache ./   

````

# Configurer PHP

````

[admin@web html]$ timedatectl
               Local time: Tue 2021-12-14 17:10:40 CET
           Universal time: Tue 2021-12-14 16:10:40 UTC
                 RTC time: Tue 2021-12-14 16:10:40
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no

````
Modification du temps dans le fichier php.ini :

````
[admin@web html]$ sudo vi /etc/opt/remi/php74/php.ini
[...]
date.timezone = "Europe/Paris"


````

# Récuperer NEXTCLOUD :

````
[admin@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  14.7M      0  0:00:10  0:00:10 --:--:-- 20.6M

[admin@web ~]$ ls
nextcloud-21.0.1.zip

[admin@web ~]$ sudo unzip nextcloud-21.0.1.zip -d /var/www/nextcloud/html/
[...]
 extracting: /var/www/nextcloud/html/nextcloud/config/CAN_INSTALL
  inflating: /var/www/nextcloud/html/nextcloud/config/config.sample.php
  inflating: /var/www/nextcloud/html/nextcloud/config/.htaccess
[admin@web ~]$ sudo rm nextcloud-21.0.1.zip

````

# Résolution DNS : 

ligne ajoutée : 10.2.1.11 web.tp2.cesi # tp2-linux




## Sécurisation : 

````
sudo vim /etc/ssh/sshd_config

PermitRootLogin = no

KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-512,rsa-sha2-256

````

# Installation et configuration fail2ban

````

sudo systemctl start firewalld

sudo dnf install epel-release

sudo dnf install fail2ban fail2ban-firewalld

sudo systemctl start fail2ban
sudo systemctl enable fail2ban

sudo vim /etc/fail2ban/jail.local


````

1. Reverse Proxy


````
[admin@proxy ~]$ sudo dnf install nginx

Complete!


````



